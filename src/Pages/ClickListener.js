import React from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { Container, Grid, } from '@material-ui/core';
export default function ClickAway(){
    const [open, setOpen] = React.useState(false);
    const [open1, setOpen1] = React.useState(false);
    const handleClick = () => {
    setOpen((prev) => !prev);
  };

  const handleClickAway = () => {
    setOpen(false);
  };
  const handleClick1 = () => {
    setOpen1((prev) => !prev);
  };

  const handleClickAway1 = () => {
    setOpen1(false);
  };
 return(
    <div className='maindiv'>
    <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
    <Grid item xs={3}>
    <ClickAwayListener onClickAway={handleClickAway}>
      <div style={{position:'relative'}}>
        <button type="button" onClick={handleClick}>
          Open menu dropdown
        </button>
        {open ? (
          <div className='dropdown'>
            Click me, I will stay visible until you click outside.
          </div>
        ) : null}
      </div>
    </ClickAwayListener>
    </Grid>
    <Grid item xs={3}>
    <ClickAwayListener
      mouseEvent="onMouseDown"
      touchEvent="onTouchStart"
      onClickAway={handleClickAway1}
    >
      <div >
        <button type="button" onClick={handleClick1}>
          Open menu dropdown
        </button>
        {open1 ? (
          <div className='dropdown'>
            Click me, I will stay visible until you click outside.
          </div>
        ) : null}
      </div>
    </ClickAwayListener>
    </Grid>
    </Container>
    </div>
 );
}