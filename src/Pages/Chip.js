import React from 'react';
import {Avatar, Grid, Chip, Paper, Container} from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import flower from '../Assets/images/1.jpg';

export default function ChipPage(){
    const handleDelete = () => {
    console.info('You clicked the delete icon.');
    };
    const handleClick = () => {
    console.info('You clicked the Chip.');
    };
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper style={{padding:'10px'}} >
            <Chip label="Basic" />
            <Chip label="Disabled" disabled />
            <Chip avatar={<Avatar>M</Avatar>} label="Clickable" onClick={handleClick} />
            <Chip
            avatar={<Avatar alt="Natacha" src={flower} />}
            label="Deletable"
            onDelete={handleDelete}
            />
            <Chip
            icon={<FaceIcon />}
            label="Clickable deletable"
            onClick={handleClick}
            onDelete={handleDelete}
            />
            <Chip
            label="Custom delete icon"
            onClick={handleClick}
            onDelete={handleDelete}
            deleteIcon={<DoneIcon />}
            />
            <Chip label="Clickable Link" component="a" href="#chip" clickable />
            <Chip
            avatar={<Avatar>M</Avatar>}
            label="Primary clickable"
            clickable
            color="primary"
            onDelete={handleDelete}
            deleteIcon={<DoneIcon />}
            />
            <Chip
            icon={<FaceIcon />}
            label="Primary clickable"
            clickable
            color="primary"
            onDelete={handleDelete}
            deleteIcon={<DoneIcon />}
            />
            <Chip label="Deletable primary" onDelete={handleDelete} color="primary" />
            <Chip
            icon={<FaceIcon />}
            label="Deletable secondary"
            onDelete={handleDelete}
            color="secondary"
            />
            </Paper>
            </Grid>
            <Grid item xs={3}>
            <Paper style={{padding:'10px'}}>
            <Chip label="Basic" variant="outlined" />
            <Chip label="Disabled" disabled variant="outlined" />
            <Chip
                avatar={<Avatar>M</Avatar>}
                label="Clickable"
                onClick={handleClick}
                variant="outlined"
            />
            <Chip
                avatar={<Avatar alt="Natacha" src={flower} />}
                label="Deletable"
                onDelete={handleDelete}
                variant="outlined"
            />
            <Chip
                icon={<FaceIcon />}
                label="Clickable deletable"
                onClick={handleClick}
                onDelete={handleDelete}
                variant="outlined"
            />
            <Chip
                label="Custom delete icon"
                onClick={handleClick}
                onDelete={handleDelete}
                deleteIcon={<DoneIcon />}
                variant="outlined"
            />
            <Chip label="Clickable link" component="a" href="#chip" clickable variant="outlined" />
            <Chip
                avatar={<Avatar>M</Avatar>}
                label="Primary clickable"
                clickable
                color="primary"
                onDelete={handleDelete}
                deleteIcon={<DoneIcon />}
                variant="outlined"
            />
            <Chip
                icon={<FaceIcon />}
                label="Primary clickable"
                clickable
                color="primary"
                onDelete={handleDelete}
                deleteIcon={<DoneIcon />}
                variant="outlined"
            />
            <Chip label="Deletable primary" onDelete={handleDelete} color="primary" variant="outlined" />
            <Chip
                icon={<FaceIcon />}
                label="Deletable secondary"
                onDelete={handleDelete}
                color="secondary"
                variant="outlined"
            />
            </Paper>
            </Grid>
             
        </Container>
        </div>
    );
}