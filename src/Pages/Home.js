import React, {useState} from 'react';
import {Button, Paper, Grid, Container, Typography,IconButton  }from '@material-ui/core';
import '../Assets/common.css';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import PhoneIcon from '@material-ui/icons/Phone';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import AppleIcon from '@material-ui/icons/Apple';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';   
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { useHistory } from 'react-router-dom';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import AddIcon from '@material-ui/icons/Add';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function Home(){
    const history = useHistory();
    const [state, setState] = useState({
    checkedA: true,
    checkedB: true,
    });
    let Time = new Date().toLocaleTimeString();
    let CDate = new Date().toLocaleDateString();

    const [ctime, setCtime] = useState(Time);
    const [cdate, setCdate] = useState(CDate);

    const UpdateTime = () =>{ 
    Time = new Date().toLocaleTimeString();
    CDate = new Date().toLocaleDateString();
    setCtime(Time);
    setCdate(CDate);
    };
    setInterval(UpdateTime, 1000);
    let crtDate = new Date();
    crtDate = crtDate.getHours();
    let message = "";
    let cssStyle = {};
    if(crtDate >1 && crtDate <12){
      message = "Good Morning";
      cssStyle.color="#7deb34";
      cssStyle.fontSize=20;
    }else if(crtDate >=12 && crtDate <17){
      message = "Good AfterNoon";
      cssStyle.color="#ff9d00";
      cssStyle.fontSize=20;
    }else if(crtDate >=17 && crtDate <22){
      message = "Good Evening";
      cssStyle.color="#ffee00";
      cssStyle.fontSize=18;
    }else{
      message = "Good Night";
      cssStyle.color="#00ffe5";
      cssStyle.fontSize=16;
    }
    const handleChange = (event) => {
      setState({ ...state, [event.target.name]: event.target.checked });
    };
    return(
        <div className="maindiv">
        
        <Container disableGutters maxWidth={'xl'} className='container'>
        <Grid className="gridHead" item xs={12}>
        <Typography className="welcometxt">Welcome to Material Ui Components</Typography>
        </Grid>
        
        <Grid>
        <div className="gridHead" item xs={12}>
        <h2>Hello, <span style={cssStyle}>{message}!</span></h2>
        </div>
        <div className="DateTimediv">
        <h2>Time: {ctime}</h2>
        <h2>Date: {cdate}</h2>
        </div>
        </Grid>
        <Grid container spacing={2}>
        <Grid item xs={3}>
        <Paper className="paper">
        <Button className="btn" variant='contained' >Submit</Button>
        <IconButton color="primary"  component="span">
          <PhotoCamera />
        </IconButton>
        <IconButton color="primary"  component="span">
        <PhoneIcon/>
        </IconButton>
        <IconButton color="primary"  component="span">
        <PhotoLibraryIcon/>
        </IconButton>
        <IconButton color="primary"  component="span">
        <AppleIcon/>
        </IconButton>
        <IconButton color="primary"  component="span">
        <ArrowBackIcon/>
        </IconButton>
        <IconButton color="primary"  component="span">
        <ArrowForwardIcon/>
        </IconButton>
        
        <Fab variant="extended">
        <NavigationIcon className='btn' />
        Navigate
        </Fab>
        <Fab color="primary" aria-label="add">
        <AddIcon />
        </Fab>
        <FormGroup row>
        <Switch
        checked={state.checkedA}
        onChange={handleChange}
        name="checkedA"
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      />
      <Switch
        checked={state.checkedB}
        onChange={handleChange}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
        
      />
      </FormGroup>
      <FormGroup row>
      <FormControlLabel
        control={<Switch checked={state.checkedA} onChange={handleChange} name="checkedA" />}
        label="Secondary"
      />
      <FormControlLabel
        control={
          <Switch
            checked={state.checkedB}
            onChange={handleChange}
            name="checkedB"
            color="primary"
          />
        }
        label="Primary"
      />
      </FormGroup>

        </Paper>
        </Grid>
        <Grid item xs={3} >
        <Paper className="paper" >
        <Button className="btn" variant='contained' onClick={() => history.push('/gridlist')}>Grid List</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/slider')}>Slider</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/complexbtn')}>Complex_Btn</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/Chip')} >Chip</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/datetime')}>DateTime</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/radioquiz')}>Radio Quiz</Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/selectBtn')}>Select </Button>
        <Button className="btn" variant='contained' onClick={() => history.push('/Breadcrumbs')} >Breadcrumb</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/popover')}>PopOver</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/popper')}>Popper</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/portal')}>Portal</Button>
        </Paper>
        </Grid>

        <Grid item xs={3}>
        <Paper className="paper">
        <Button className="btn" variant='contained' onClick={() => history.push('/todolist')} >ToDo List</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/accordion')}>Accordion</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/CardPage')}>Card Page</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/BadgePage')}>Badge</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/SnackBar')}>SnackBar</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/TopBar')}>TopBar</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/List')}>List</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/Tooltip')}>ToolTip</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/Avatar')}>Avatar</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/modal')}>Modal</Button>
        <Button className='btn' variant='contained' onClick={() => history.push('/Clickaway')}>Click away listener</Button>
        
        </Paper>
        </Grid>

        <Grid item xs={3}>
        <Paper className="paper">
        <Button  variant='outlined' color='secondary'>Submit</Button>
        </Paper>
        </Grid>

        </Grid>
        </Container>
        </div>
    );
}