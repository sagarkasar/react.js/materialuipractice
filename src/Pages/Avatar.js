import React from 'react';
import {Avatar,Container, Grid, Paper, Badge} from '@material-ui/core';
import flower from '../Assets/images/3.jpg';
import flower1 from '../Assets/images/1.jpg';
import FolderIcon from '@material-ui/icons/Folder';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { withStyles } from '@material-ui/core/styles';

export default function AvatarPage(){
    const SmallAvatar = withStyles((theme) => ({
    root: {
    width: 22,
    height: 22,
    border: `2px solid ${theme.palette.background.paper}`,
    },
    }))(Avatar);
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper style={{display:'flex', padding:'10px'}} >
            <Avatar alt="Remy Sharp" src={flower} />
            <Avatar>S</Avatar>
            <Avatar style={{backgroundColor:'grey'}}><FolderIcon /></Avatar>
            <Avatar variant="square" style={{backgroundColor:'orange'}}>
            N
            </Avatar>
            <Avatar variant="rounded" style={{backgroundColor:'Green'}}>
            <AssignmentIcon />
            </Avatar>

            <Badge
            overlap="circle"
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
            }}
            badgeContent={<SmallAvatar alt="Remy Sharp" src={flower} />}
            >
            <Avatar alt="Travis Howard" src={flower1} />
            </Badge>
            </Paper>
            </Grid>
        </Container>
        </div>
    );
}