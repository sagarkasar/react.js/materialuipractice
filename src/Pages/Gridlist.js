import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import tileData from '../Pages/tileData';


export default function Gridlist(){
    return(
        <div className='root'>
        <GridList cellHeight={180} className='gridList' cols={3}>
        {tileData.map((tile) => (
        <GridListTile key={tile.img} cols={tile.cols || 1}>
            <img src={tile.img} alt={tile.title} />
          </GridListTile>
        ))}
        </GridList>
        </div>
    );
}