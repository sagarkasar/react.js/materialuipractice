import React,{useState} from 'react';
import {Paper, Grid, Container,Fab, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

export default function TodoList(){
    const [inputlist, setInputlist] = useState('');
    const [saveitems, setSaveitems] = useState([]);

    const itemEvent = (event) =>{
        setInputlist(event.target.value)
    }
    const listofitems = () => {
        setSaveitems((oldlist)=>{
            return [...oldlist,inputlist];
        });
        setInputlist('');
    }
    const closelist = (id) =>{
        setSaveitems((oldlist)=>{
            return oldlist.filter((arr, index)=>{
                return index !== id;
            });
        });
    }
    return(
        <div className="maindiv">
        <Container disableGutters maxWidth={'xl'} className='container'>
        <Grid className="gridHead" item xs={12}>
            <h1 className='todoheader'>Todo List</h1>
        </Grid>
        <Grid container spacing={2}>
        <Paper className='paper'>
        <TextField label="Add ToDo List" value={inputlist} variant="outlined" onChange={itemEvent}/>
        <Fab color="primary" aria-label="add">
        <AddIcon onClick={listofitems} />
        </Fab>

        <ul>
           { saveitems.map((value, index)=>{
                return (
                    <div>
                    <div className='listdiv'>
                    <CloseIcon fontSize='small' className='closeicon' key={index} id={index} onClick={() => closelist(index)} />
                    <li type="none" >{value}</li>
                    </div>
                    </div>
                );
            })}
        </ul>
        </Paper>
        </Grid>
        </Container>
        </div>
    );
}