import React from 'react';
import {Grid, Paper, Container} from '@material-ui/core';
import Popper from '@material-ui/core/Popper';

export default function PopperPage(){
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popper' : undefined;

    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper style={{padding:'10px'}} >
            <div>
            <button aria-describedby={id} type="button" onClick={handleClick}>
                Toggle Popper
            </button>
            <Popper id={id} open={open} anchorEl={anchorEl}>
                <div className='bodydiv'>The content of the Popper.</div>
            </Popper>
            </div>
            </Paper>
            </Grid>
        </Container>
        </div>
    );
}


