import React from 'react';
import { Grid, Container, Paper} from '@material-ui/core';
import Portal from '@material-ui/core/Portal';

export default function PortalPage(){
    const [show, setShow] = React.useState(false);
    const container = React.useRef(null);

    const handleClick = () => {
        setShow(!show);
    };
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper style={{padding:'10px'}} >
            <div>
            <button type="button" onClick={handleClick}>
                {show ? 'Unmount children' : 'Mount children'}
            </button>
            <div className='alert'>
                It looks like I will render here.
                {show ? (
                <Portal container={container.current}>
                    <span>But I actually render here!</span>
                </Portal>
                ) : null}
            </div>
            <div className='alert' ref={container} />
            </div>
            </Paper>
            </Grid>
        </Container>
        </div>
    );
}