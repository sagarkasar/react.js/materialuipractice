import React from 'react';
import { Container, Badge } from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';


export default function BadgePage(){
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' spacing={2}>
        <Badge badgeContent={99} color="primary">
        <MailIcon />
        </Badge>
         <Badge badgeContent={4} color="secondary">
        <MailIcon />
        </Badge>
        <Badge badgeContent={4} color="error">
        <MailIcon />
        </Badge>
        
        </Container>
        </div>
    );
}