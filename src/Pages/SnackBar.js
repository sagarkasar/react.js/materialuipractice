import React from 'react'
import { Container, Button, Snackbar, IconButton,  } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
export default function SnackBar(){
    const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container'>
        <Button variant='outlined' onClick={handleClick}>Open simple snackbar</Button>
        <Snackbar
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
            }}
            open={open}
            autoHideDuration={3000}
            onClose={handleClose}
            message="Note archived"
            action={
            <React.Fragment>
                <Button color="secondary" size="small" onClick={handleClose}>
                UNDO
                </Button>
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                <CloseIcon fontSize="small" />
                </IconButton>
            </React.Fragment>
            }
        />
        </Container>
        </div>
    );
}