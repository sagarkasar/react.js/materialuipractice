import React from 'react';
import {Typography, Link, Paper} from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import HomeIcon from '@material-ui/icons/Home';
import WhatshotIcon from '@material-ui/icons/Whatshot';
import GrainIcon from '@material-ui/icons/Grain';

export default function BreadcrumbsPage(){

    function handleClick(event) {
    event.preventDefault();
    console.info('You clicked a breadcrumb.');
    }

    return(
        <div className='maindiv'>
        <Paper className='paper'>
        <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" onClick={handleClick} >
        <HomeIcon style={{marginRight:'5px',height:'17px',width:'17px'}} />
        Material-UI
      </Link>
      <Link
        color="inherit"
        href="/getting-started/installation/"
        onClick={handleClick}
        
      >
        <WhatshotIcon style={{marginRight:'5px',height:'17px',width:'17px'}} />
        Core
      </Link>
      <Typography color="textPrimary" >
        <GrainIcon style={{marginRight:'5px',height:'17px',width:'17px'}} />
        Breadcrumb
      </Typography>
    </Breadcrumbs>
    </Paper>

    <Paper className='paper'>
        <Breadcrumbs aria-label="breadcrumb">
  <Link color="inherit" href="/" onClick={handleClick}>
    Material-UI
  </Link>
  <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}>
    Core
  </Link>
  <Link
    color="textPrimary"
    href="/components/breadcrumbs/"
    onClick={handleClick}
    aria-current="page"
  >
    Breadcrumb
  </Link>
    </Breadcrumbs>  
    </Paper>

    <Paper className='paper'>
    <Breadcrumbs maxItems={2} aria-label="breadcrumb">
      <Link color="inherit" href="#" onClick={handleClick}>
        Home
      </Link>
      <Link color="inherit" href="#" onClick={handleClick}>
        Catalog
      </Link>
      <Link color="inherit" href="#" onClick={handleClick}>
        Accessories
      </Link>
      <Link color="inherit" href="#" onClick={handleClick}>
        New Collection
      </Link>
      <Typography color="textPrimary">Belts</Typography>
    </Breadcrumbs>
    </Paper>
    
        </div>
    );
}