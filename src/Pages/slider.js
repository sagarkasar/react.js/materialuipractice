import React from 'react';
import { Paper,Grid,Container, Typography} from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';


export default function Sliderpage(){
    const [value, setValue] = React.useState(30);
    const handleChange = (event, newValue) => {
    setValue(newValue);
    };
    
    const marks = [
  {
    value: 0,
    label: '0°C',
  },
  {
    value: 20,
    label: '20°C',
  },
  {
    value: 37,
    label: '37°C',
  },
  {
    value: 100,
    label: '100°C',
  },
];

function valuetext(value) {
  return `${value}°C`;
}
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container'>
            <Grid item xs={12}>
                <Paper className='paper' spacing={2}>
                    <Grid container spacing={2} style={{width:'200px'}}>
                    <Grid item>
                    <VolumeDown />
                    </Grid>
                    <Grid item md>
                    <Slider value={value} onChange={handleChange} aria-labelledby="continuous-slider" />
                    </Grid>
                    <Grid item>
                    <VolumeUp />
                    </Grid>
                    </Grid>
                </Paper>
                <Paper className='paper' spacing={2}>
                    <Grid container spacing={2} style={{width:'300px', padding:'20px'}}>
                    <Typography id="track-inverted-slider" gutterBottom>
                    Inverted track
                    </Typography>
                    <Slider
                    track="inverted"
                    aria-labelledby="track-inverted-slider"
                    getAriaValueText={valuetext}
                    defaultValue={30}
                    marks={marks}
                    />
                    </Grid>
                </Paper>
                <Paper className='paper' spacing={2}>
                 <Grid container spacing={2} style={{width:'200px', padding:'20px'}}>
                  <div >
                  <Typography id="range-slider" gutterBottom>
                    Temperature range
                  </Typography>
                  <Slider
                    value={value}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                  />
                </div>
                </Grid>
                </Paper>
            </Grid>
            </Container>
        </div>
    );
}