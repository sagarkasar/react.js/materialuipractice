import React from 'react';
import {Grid, Container,Accordion,AccordionSummary,Typography,AccordionDetails} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
export default function Accordionpage(){
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container'>
        <Grid className="gridHead" item xs={12}>
        <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography >Accordion 1</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
            sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
        </Accordion>
        </Grid>
        </Container>
        </div>
    )
}