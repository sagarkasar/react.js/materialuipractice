import React from 'react';
import {Card, Container, Grid} from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Img from '../Assets/images/6.jpg';
 function CardPage(){
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' spacing={2}>
        <Grid item xs={3} >
        <Card className='cardroot'>
        <CardActionArea>
        <CardMedia
        className='media'
        image={Img}
        title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Lizard
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
        </CardActionArea>
        <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
        </CardActions>
        </Card>
        </Grid>
        
        </Container>
        </div>
    );
}
export default CardPage;