import Image1 from '../Assets/images/1.jpg'; 
import Image2 from '../Assets/images/2.jpg';
import Image3 from '../Assets/images/3.jpg';
import Image4 from '../Assets/images/4.jpg';
import Image5 from '../Assets/images/5.jpg';
import Image6 from '../Assets/images/6.jpg';

const tileData = [
    {
      img: Image1,
      title: 'Image',
      author: 'author',
      cols: 2,
      width:'30%',
    },
    {
      img: Image2,
      title: 'Image',
      author: 'author',
      cols: 1,
      width:'20%',
    },
    {
      img: Image3,
      title: 'Image',
      author: 'author',
      cols: 1,
      width:'20%',
    },
    {
      img: Image4,
      title: 'Image',
      author: 'author',
      cols: 1,
      width:'25%',
    },
    {
      img: Image5,
      title: 'Image',
      author: 'author',
      cols: 1,
      width:'30%',
    },
    {
      img: Image6,
      title: 'Image',
      author: 'author',
      cols: 3,
      width:'20%',
    },
  ];

  export default tileData;
  