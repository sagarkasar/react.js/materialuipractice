import React from 'react';
import {Grid, Paper, Container,Button} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

export default function Tooltiop(){
    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
    setOpen(false);
    };
    const handleOpen = () => {
    setOpen(true);
    };
    return(
        <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper  className='paper'>
            <div>
            <Tooltip title="Delete">
                <IconButton aria-label="delete" >
                <DeleteIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Add" aria-label="add" arrow>
                <Fab color="primary" >
                <AddIcon />
                </Fab>
            </Tooltip>
            </div>
            </Paper>
            </Grid>
            <Grid item xs={3}>
            <Paper  className='paper'>
            <div>
            <Tooltip title="Add">
                <Button>Light</Button>
            </Tooltip>
            <Tooltip title="Add">
                <Button>Bootstrap</Button>
            </Tooltip>
            </div>
            </Paper>
            </Grid>
            <Grid item xs={3}>
            <Paper  className='paper'>
            <div>
             <Grid item>
            <Tooltip disableFocusListener title="Add">
                <Button>Hover or touch</Button>
            </Tooltip>
            </Grid>
            <Grid item>
            <Tooltip disableHoverListener title="Add">
                <Button>Focus or touch</Button>
            </Tooltip>
            </Grid>
            <Grid item>
            <Tooltip disableFocusListener disableTouchListener title="Add">
                <Button>Hover</Button>
            </Tooltip>
            </Grid>
            </div>
            </Paper>
            </Grid>
            <Grid item xs={3}>
            <Paper  className='paper'>
            <Tooltip open={open} onClose={handleClose} onOpen={handleOpen} title="Add">
            <Button>Controlled</Button>
            </Tooltip>
            </Paper>
            </Grid>
        </Container>
        </div>
    );
}