import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import React from 'react';
import { Paper,Grid} from '@material-ui/core';
import Input from '@material-ui/core/Input';
import Chip from '@material-ui/core/Chip';
import { useTheme } from '@material-ui/core/styles';



export default function SelectButton(){
    const [age, setAge] = React.useState('');
    const [personName, setPersonName] = React.useState([]);
    const handleChange = (event) => {
    setAge(event.target.value);
    setPersonName(event.target.value);
    };
    
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];
const theme = useTheme();
function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}
    return(
        <div className="maindiv">
            <Grid>
            <Paper className='paper'>
            <FormControl >
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Age
            </InputLabel>
            <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={age}
            onChange={handleChange}
            displayEmpty
            
            >
            <MenuItem value="">
                <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
            </Select>
            <FormHelperText>Label + placeholder</FormHelperText>
            </FormControl>
            </Paper>
            <Paper className='paper'>
                <FormControl variant="outlined" >
                <InputLabel htmlFor="outlined-age-native-simple">Age</InputLabel>
                <Select
                native
                value={age}
                onChange={handleChange}
                label="Age"
                
                >
                <option aria-label="None" value="" />
                <option value={10}>Ten</option>
                <option value={20}>Twenty</option>
                <option value={30}>Thirty</option>
                </Select>
                </FormControl>
            </Paper>
            <Paper className='paper'>
            <FormControl >
        <InputLabel id="demo-mutiple-chip-label">Chip</InputLabel>
        <Select
          labelId="demo-mutiple-chip-label"
          id="demo-mutiple-chip"
          multiple
          value={personName}
          onChange={handleChange}
          input={<Input id="select-multiple-chip" />}
          renderValue={(selected) => (
            <div >
              {selected.map((value) => (
                <Chip key={value} label={value}  />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {names.map((name) => (
            <MenuItem key={name} value={name} style={getStyles(name, personName, theme)}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      </Paper>
            </Grid>
        </div>
    );
}