import React from 'react';
import { Grid, Container, Paper} from '@material-ui/core';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


export default function PopOver(){
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
    setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return(
    <div className='maindiv'>
        <Container disableGutters maxWidth={'xl'} className='container' style={{display:'flex', justifyContent:'space-around' }}>
            <Grid item xs={3}>
            <Paper style={{padding:'10px'}} >
            <div>
            <Button aria-describedby={id} variant="contained" color="primary" onClick={handleClick}>
                Open Popover
            </Button>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
                }}
                transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
                }}
            >
                <Typography>The content of the Popover.</Typography>
            </Popover>
            </div>
            </Paper>
            </Grid>
        </Container>
    </div>
    );
}