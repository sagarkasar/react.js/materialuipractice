import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './Pages/Home';
import Gridlist from './Pages/Gridlist';
import Complexbtn from './Pages/Complexbutton'
import DateTime from './Pages/DateTime';
import RadioQuiz from './Pages/RadioQuiz';
import SelectButton from './Pages/SelectButton'
import Slider from './Pages/slider';
import Breadcrumbs from './Pages/Breadcrumbs';
import TodoList from './Pages/todolist';
import Accordion from './Pages/Accordion';
import CardPage from './Pages/CardPage';
import BadgePage from './Pages/BadgePage';
import SnackBar from './Pages/SnackBar';
import Design from './Pages/Design';
import TopBar from './Pages/TopBar';
import List from './Pages/List';
import Tooltip from './Pages/Tooltip'; 
import Chip from './Pages/Chip';
import Avatar from './Pages/Avatar';
import ClickAway from './Pages/ClickListener';
import Modal from './Pages/Modal';
import PopOver from './Pages/PopOver';
import Popper from './Pages/Popper';
import PortalPage from './Pages/Portal';

function App() {
  return (
    <Router>
    <Route path='/' exact component={Home} />
    <Route path='/gridlist' exact component={Gridlist} />
    <Route path='/complexbtn' exact component={Complexbtn} />
    <Route path='/datetime' exact component={DateTime} />
    <Route path='/radioquiz' exact component={RadioQuiz} />
    <Route path='/selectbtn' exact component={SelectButton} />
    <Route path='/slider' exact component={Slider} />
    <Route path='/Breadcrumbs' exact component={Breadcrumbs} />
    <Route path='/Todolist' exact component={TodoList} />
    <Route path='/accordion' exact component={Accordion} />
    <Route path='/CardPage' exact component={CardPage} />
    <Route path='/BadgePage' exact component={BadgePage} />
    <Route path='/SnackBar' exact component={SnackBar} />
    <Route path='/design' exact component={Design} />
    <Route path='/TopBar' exact component={TopBar} />
    <Route path='/List' exact component={List} />
    <Route path='/Tooltip' exact component={Tooltip} />
    <Route path='/chip' exact component={Chip} />
    <Route path='/avatar' exact component={Avatar} />
    <Route path='/Clickaway' exact component={ClickAway} />
    <Route path='/modal' exact component={Modal} />
    <Route path='/popover' exact component={PopOver} />
    <Route path='/popper' exact component={Popper} />
    <Route path='/portal' exact component={PortalPage} />
    </Router>
  );
}

export default App;
